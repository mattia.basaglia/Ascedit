/**
 * \file
 * \author Mattia Basaglia
 * \copyright Copyright 2015-2016 Mattia Basaglia
 * \section License
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_MODULE Test_Document

#include <boost/test/unit_test.hpp>

#include "document/document.hpp"

using namespace doc;

BOOST_AUTO_TEST_CASE( test_layer_point_cmp )
{
    auto cmp = Layer::QPointCmp();
    BOOST_CHECK(cmp(QPoint(10, 1), QPoint(6, 2)));
    BOOST_CHECK(cmp(QPoint(10, 1), QPoint(11, 1)));
    BOOST_CHECK(!cmp(QPoint(10, 1), QPoint(10, 1)));
    BOOST_CHECK(!cmp(QPoint(10, 3), QPoint(6, 2)));
}

BOOST_AUTO_TEST_CASE( test_layer_char )
{
    Layer layer(0);
    BOOST_CHECK_EQUAL(layer.to_string(), "");
    layer.set_char({0, 0}, 'P');
    BOOST_CHECK_EQUAL(layer.to_string(), "P");
    layer.set_char({1, 1}, '!');
    layer.set_char({2, 0}, 'o');
    BOOST_CHECK_EQUAL(layer.to_string(), "P o\n!");
    BOOST_CHECK_EQUAL(layer.char_at({2, 0}), 'o');
    BOOST_CHECK_EQUAL(layer.char_at({20, 20}), ' ');
    layer.remove_char({2, 0});
    BOOST_CHECK_EQUAL(layer.to_string(), "P\n!");
    layer.remove_char({2, 0});
    layer.set_char({1, 1}, ' ');
    BOOST_CHECK_EQUAL(layer.to_string(), "P");
}

BOOST_AUTO_TEST_CASE( test_layer_color )
{
    Layer layer(0);
    unsigned changed_to = 0;
    bool changed = false;
    QObject::connect(&layer, &Layer::color_changed, [&changed_to,&changed](unsigned col){
        changed_to = col;
        changed = true;
    });

    BOOST_CHECK_EQUAL(layer.color(), 0);

    layer.set_color(1);
    BOOST_CHECK_EQUAL(layer.color(), 1);
    BOOST_CHECK(changed);
    BOOST_CHECK_EQUAL(changed_to, 1);

    changed = false;
    layer.set_color(1);
    BOOST_CHECK(!changed);
}

BOOST_AUTO_TEST_CASE( test_layer_name )
{
    Layer layer(0);
    QString changed_to;
    bool changed = false;
    QObject::connect(&layer, &Layer::name_changed,
        [&changed_to, &changed](QString v)
        {
            changed_to = v;
            changed = true;
        }
    );

    BOOST_CHECK(layer.name() == "");

    layer.set_name("foo");
    BOOST_CHECK(layer.name() == "foo");
    BOOST_CHECK(changed);
    BOOST_CHECK(changed_to == "foo");

    changed = false;
    layer.set_name("foo");
    BOOST_CHECK(!changed);
}

BOOST_AUTO_TEST_CASE( test_layer_effective_size )
{
    Layer layer(0);
    BOOST_CHECK(layer.effective_size() == QSize(0, 0));
    layer.set_char(QPoint(0, 0), 'o');
    BOOST_CHECK(layer.effective_size() == QSize(1, 1));
    layer.set_char(QPoint(7, 9), 'o');
    BOOST_CHECK(layer.effective_size() == QSize(8, 10));
    layer.set_char(QPoint(6, 10), 'o');
    BOOST_CHECK(layer.effective_size() == QSize(8, 11));
}

BOOST_AUTO_TEST_CASE( test_layer_swap )
{
    Layer a(1, "one");
    a.set_char(QPoint(1, 2), '3');

    Layer b(5, "five");
    b.set_char(QPoint(3, 4), '5');

    a.swap(b);

    BOOST_CHECK(a.color() == 5);
    BOOST_CHECK(b.color() == 1);

    BOOST_CHECK(a.name() == "five");
    BOOST_CHECK(b.name() == "one");

    BOOST_CHECK_EQUAL(a.to_string(), "\n\n\n\n  5");
    BOOST_CHECK_EQUAL(b.to_string(), "\n\n3");

}
