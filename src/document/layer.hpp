/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ASCEDIT_LAYER_HPP
#define ASCEDIT_LAYER_HPP

#include <map>
#include <string>

#include <QObject>
#include <QPoint>
#include <QSize>

namespace doc {

/**
 * \brief Layer for an ASCII art document
 */
class Layer : public QObject
{
private:
    Q_OBJECT
    /**
     * \brief Color index in the parent palette of the parent document
     */
    Q_PROPERTY(unsigned color READ color WRITE set_color NOTIFY color_changed)
    /**
     * \brief Layer name
     */
    Q_PROPERTY(QString name READ name WRITE set_name NOTIFY name_changed)

public:
    /**
     * \brief Comparator functor to sort positions correctly in the map
     */
    struct QPointCmp
    {
        bool operator()(const QPoint& a, const QPoint& b) const
        {
            return a.y() < b.y() || (a.y() == b.y() && a.x() < b.x());
        }
    };

    /**
     * \brief Map type to hold the characters in the layer
     * \todo Pick the right mapping type
     */
    typedef std::map<QPoint, char, QPointCmp> CharacterMap;

    Layer(unsigned color, QString name = "", QObject* parent = nullptr)
        : QObject(parent), _color(color), _name(name)
    {}

    unsigned color() const
    {
        return _color;
    }

    const CharacterMap& characters() const
    {
        return _characters;
    }

    /**
     * \brief Sets a character at a given position
     * \param pos The position of the character
     * \param ch  Character to set
     *
     * If \p ch is a space or control character,
     * the character will be erased instead
     */
    void set_char(QPoint pos, char ch)
    {
        // all ascii characters less than plain space are either
        // spaces or special
        if ( ch <= ' ' )
            remove_char(pos);
        else
            _characters[pos] = ch;
    }

    /**
     * \brief Removes the character at the given position
     */
    void remove_char(QPoint pos)
    {
        _characters.erase(pos);
    }

    /**
     * \brief Return the character at the given position
     *
     * If there is no character at that position, it will return a space
     */
    char char_at(QPoint pos) const
    {
        auto it = _characters.find(pos);
        return it == _characters.end() ? ' ' : it->second;
    }

    /**
     * \brief Converts the layer into a string with the matching characters
     */
    std::string to_string() const
    {
        std::string ret;
        QPoint last;
        for ( const auto& pair : _characters )
        {
            if ( last.y() < pair.first.y() )
                ret += std::string(pair.first.y() - last.y(), '\n');
            if ( last.x() + 1 < pair.first.x() )
                ret += std::string(pair.first.x() - last.x() - 1, ' ');
            ret += pair.second;
        }
        return ret;
    }

    QString name() const
    {
        return _name;
    }

    /**
     * \brief Evaluates the size of the layer from its contents
     */
    QSize effective_size() const
    {
        QSize sz(0, 0);
        for ( const auto& point: _characters )
        {
            if ( point.first.x() >= sz.width() )
                sz.setWidth(point.first.x() + 1);
            if ( point.first.y() >= sz.height() )
                sz.setHeight(point.first.y() + 1);
        }
        return sz;
    }

    /**
     * \brief Swaps the contents of two layers
     */
    void swap(Layer& other)
    {
        std::swap(_characters, other._characters);
        std::swap(_color, other._color);
        std::swap(_name, other._name);
    }

public slots:
    void set_color(unsigned color)
    {
        if ( color != _color )
        {
            _color = color;
            emit color_changed(color);
        }
    }

    void set_name(const QString& name)
    {
        if ( name != _name )
        {
            _name = name;
            emit name_changed(_name);
        }
    }

signals:
    void color_changed(unsigned color);
    void name_changed(const QString& name);

private:
    CharacterMap _characters;
    unsigned     _color;
    QString     _name;
};

} // namespace doc
#endif // ASCEDIT_LAYER_HPP
