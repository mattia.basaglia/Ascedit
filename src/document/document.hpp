/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ASCEDIT_DOCUMENT_HPP
#define ASCEDIT_DOCUMENT_HPP

#include "color_palette.hpp"

#include "layer.hpp"

/**
 * \brief Namespace to group functionality related to the document structure
 */
namespace doc {

/**
 * \brief Palette output mode
 */
enum class PaletteFormat
{
    Plain = -1, //< Only the basic 8 colors are supported
    BrightBold, //< Bright colors are achieved by making them bold
    BrightExt,  //< Bright colors are achieved with the bright extension
    PaletteExt, //< Use the xterm 256-color palette
    FullColor,  //< Use the full RGB extension
};

namespace default_palette {
    extern color_widgets::ColorPalette colors8_dark;
    extern color_widgets::ColorPalette colors8_bright;
    extern color_widgets::ColorPalette colors16;
    extern color_widgets::ColorPalette colors256;
}


/**
 * \brief A document for ASCII art data
 */
class Document : QObject
{
private:
    Q_OBJECT
    Q_ENUMS(PaletteFormat)

    /**
     * \brief Document layers
     */
    Q_PROPERTY(QList<Layer*> layers READ layers NOTIFY layers_changed)
    /**
     * \brief Size of the canvas boundaries
     * \note Layers might have a different effective size
     */
    Q_PROPERTY(QSize size READ size WRITE set_size NOTIFY size_changed)
    /**
     * \brief Human-readable name of the document
     */
    Q_PROPERTY(QString name READ name WRITE set_name NOTIFY name_changed)
    /**
     * \brief Full path to the file used to store this document
     */
    Q_PROPERTY(QString file_name READ file_name WRITE set_file_name NOTIFY file_name_changed)

    /**
     * \brief Palette output mode
     */
    Q_PROPERTY(PaletteFormat palette_format READ palette_format
        WRITE set_palette_format NOTIFY palette_format_changed)

public:
    const QList<Layer*>& layers() const
    {
        return _layers;
    }

    QSize size() const
    {
        return _size;
    }

    QString name() const
    {
        return _name;
    }

    QString file_name() const
    {
        return _file_name;
    }

    const color_widgets::ColorPalette& palette() const
    {
        return _palette;
    }

    color_widgets::ColorPalette& palette()
    {
        return _palette;
    }

    /**
     * \brief Evaluates the size of the document from its contents
     */
    QSize effective_size() const
    {
        QSize sz(0, 0);
        for ( const Layer* layer: _layers )
        {
            sz = sz.expandedTo(layer->effective_size());
        }
        return sz;
    }

    PaletteFormat palette_format() const
    {
        return _palette_format;
    }

    QColor layer_color(int layer_index)
    {
        if ( layer_index >= 0 && layer_index < _layers.size() )
        {
            auto color_index = _layers[layer_index]->color();
            if ( color_index >= unsigned(_palette.count()) )
                return _palette.colorAt(color_index);
        }
        return QColor();
    }

public slots:
    /**
     * \brief Appends a new layer to the document
     */
    void add_layer(unsigned color, const QString& name = "")
    {
        _layers.push_back(new Layer(color, name, this));
        emit layers_changed(_layers);
    }

    /**
     * \brief Removes all layers
     */
    void clear_layers()
    {
        for ( auto layer : _layers )
            delete layer;
        _layers.clear();
    }

    /**
     * \brief Removes the layer at the given position
     */
    void remove_layer(int index)
    {
        if ( index >= 0 && index < _layers.size() )
        {
            delete _layers[index];
            _layers.removeAt(index);
            emit layers_changed(_layers);
        }
    }

    void set_size(const QSize& size)
    {
        if ( size != _size )
        {
            _size = size;
            emit size_changed(_size);
        }
    }

    void set_name(const QString& name)
    {
        if ( name != _name )
        {
            _name = name;
            emit name_changed(_name);
        }
    }

    void set_file_name(const QString& file_name)
    {
        if ( file_name != _file_name )
        {
            _file_name = file_name;
            emit file_name_changed(_file_name);
        }
    }

    void set_palette_format(PaletteFormat palette_format)
    {
        if ( palette_format != _palette_format )
        {
            _palette_format = palette_format;
            emit palette_format_changed(_palette_format);
        }
    }

signals:
    void layers_changed(const QList<Layer*>& layers);
    void size_changed(const QSize& size);
    void name_changed(const QString& name);
    void file_name_changed(const QString& file_name);
    void palette_format_changed(PaletteFormat palette_format);

private:
    QList<Layer*> _layers;
    QSize _size;
    QString _name;
    QString _file_name;
    color_widgets::ColorPalette _palette;
    PaletteFormat _palette_format;
};

} // namespace doc
#endif // ASCEDIT_DOCUMENT_HPP
